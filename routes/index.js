var express = require('express');
var router = express.Router();
var Constants = require('../utils/Constants');
var ExchangeDetails = require('../models/ExchangeDetails');
var DataSaving = require('../models/DataSaving');
var HelperMethod = require('../utils/HelperMethods');
var ccxt = require('ccxt');


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

function take_data_of_time(time_hour, callback) {
    const time = time_hour * 60;
    const no_bids = [];
    const no_asks = [];
    let promiseArr = [];
    DataSaving.GettingAllData((response_data) => {
        for (var i = 0; i < response_data.length; i++) {
            var single_exchangedetials = response_data[i].exchange_details;
            if (single_exchangedetials.length > time) {
                for (var j = single_exchangedetials.length; j > 60; j--) {
                    var p = new Promise((resolve, reject) => {
                        if (single_exchangedetials[j - 1]._id !== undefined) {
                            ExchangeDetails.GiveBidAsk(single_exchangedetials[j - 1]._id, (ask_bid_data) => {
                                return resolve(ask_bid_data);
                            });
                        }
                    }).catch(err => {
                        console.log("Error while fetching data " + err.message);

                    });

                    promiseArr.push(p);
                }
            }
        }
        console.log("Length of the here " + promiseArr.length)
        Promise.all(promiseArr).catch(data => {
            console.log("Data from array " + JSON.stringify(data));
        }).then(data => {

            if (data.asks.length > 0) {
                for (var i = 0; i < data.asks.length; i++) {
                    no_asks.push(data.asks[i]);
                }
            }

            if (data.bids.length > 0) {
                for (var j = 0; j < data.bids.length; j++) {
                    no_bids.push(data.bids[j])
                }
            }

            console.log("No. of Bids " + no_bids.length + " :: No of Asks" + no_asks.length);
            return callback(no_bids, no_asks);
        })
    });
}

router.get('/trade/:time_hour', (req, res, next) => {
    const {time_hour} = req.params;
    take_data_of_time(time_hour, function (bids, asks) {
        const bid_length = bids.length;
        const ask_length = asks.length;
        const bid_market_sentiments = (bid_length * 100) / (bid_length + ask_length);
        const aks_market_sentiments = (ask_length * 100) / (bid_length + ask_length);
        res.json({
            message: "market sentiments for last " + time_hour + " Hour.",
            time: time_hour,
            ask: bid_market_sentiments,
            bids: aks_market_sentiments,
        });
    });
});


function take_sell_buy_rates(time_hour, callback) {
    const time = time_hour * 60;
    const btc_price = 0;
    const exchange_name = "unknown";
    const last_updated_time = 5.32;
    const all_btc_prices = [];
    const promise_arr = [];
    DataSaving.GettingAllData((response_data) => {

        if (response_data !== undefined && response_data.length > 0) {
            for (var i = 0; i < response_data.length; i++) {
                var ex_name = response_data[i].exchange_name;
                // check if the list of the market is available
                // in the selected list of constant files markets
                console.log("exchange name " + ex_name + " ---- " + Constants.BLOCKED_EXCHNAGES);
                if (ex_name !== undefined && Constants.TOP_EXCHANGES_LIST.indexOf(ex_name) > -1 && Constants.BLOCKED_EXCHNAGES.indexOf(ex_name) === -1) {
                    const p = new Promise((resolve, reject) => {
                        var exchagne_details = response_data[i].exchange_details[response_data[i].exchange_details.length - 1];
                        var lastupdatexchangetimestamp = exchagne_details.created_at;
                        //checking if the timestamp is not more older than last 2 hours
                        var updated_exchang_document_id = exchagne_details._id;

                        ExchangeDetails.GiveLatestDataWithExchangeName(updated_exchang_document_id, ex_name, (document_details, exchange_name) => {
                            return resolve({
                                exchange_name: exchange_name,
                                exchange_data: Number(document_details.exchange_btc_price.replace("$", ""))
                            });

                        });
                    }).catch(error => {
                        console.log("Error " + error.message);
                    });

                    promise_arr.push(p);

                }
            }


            if (promise_arr.length > 0) {
                console.log(" I am batman");
                Promise.all(promise_arr)
                    .catch(console.log)
                    .then((resolved_data) => {
                        let min_price = resolved_data[0].exchange_data;
                        let max_price = resolved_data[0].exchange_data;
                        let min_position = 0, max_position = 0;

                        for (var i = 0; i < resolved_data.length; i++) {
                            if (resolved_data[i].exchange_data < min_price) {
                                min_price = resolved_data[i].exchange_data;
                                min_position = i;
                            }
                        }

                        for (var j = 0; j < resolved_data.length; j++) {
                            if (resolved_data[j].exchange_data > max_price) {
                                max_price = resolved_data[j].exchange_data;
                                max_position = j;
                            }
                        }

                        var response = {
                            minimum_btc_price: min_price,
                            minimum_price_exchange: resolved_data[min_position],
                            maximum_btc_price: max_price,
                            maximum_price_exchange: resolved_data[max_position],
                            exchange_details: resolved_data
                        }

                        callback(response)
                    })
            }
        }
    });
}

router.get('/buysellrates/:time_hour', (req, res, next) => {
    const {time_hour} = req.params;
    take_sell_buy_rates(time_hour, (exhcange_data) => {
        res.json({
            message: "Maximum and minimum Price fetched from last 1 hour",
            time: time_hour,
            data: exhcange_data
        });
    });

});


// Executing order

module.exports.creatingOrderFromExchange = async () => {

    //const {symbol, exchangeId, amount, price} = request_params;
    const symbol = 'ETH/BTC';
    const orderType = 'limit';
    const side = 'sell';
    const amount = 0.321;
    const price = 0.123;


    const bittrex_exchange = new ccxt.bittrex({
        'apiKey': Constants.BITTREX_API_KEY,
        'secret': Constants.BITTREX_SECRET_KEY,
        'verbose': false,
        'timeout': 60000,
        'enableRateLimit': true,
    });


    while (true) {
        try {
            await bittrex_exchange.loadMarkets();
        } catch (e) {
            if (e instanceof ccxt.RequestTimeout)
                console.log(bittrex_exchange.iso8601(Date.now()), e.constructor.name, e.message)
        }
    }


    try {

        var exchange_market_arry = bittrex_exchange.fetchMarkets();

        if (exchange_market_arry.length > 0) {
            console.log("Bittrex exchange markets ", exchange_market_arry);
            // const response = await bittrex_exchange.createOrder(symbol, orderType, side, amount, price);
            // console.log(response);
            // console.log('Succeeded');
        } else {

        }


    } catch (e) {
        console.log(bittrex_exchange.iso8601(Date.now()), e.constructor.name, e.message)
        console.log('Failed');
    }
}


router.get('/createlimitorderbittrex/:symbol/:exchangeId/:amount/:price', (req, res, next) => {

    creatingOrderFromExchange(req.params, (response_data) => {


    })
});


module.exports = router;
