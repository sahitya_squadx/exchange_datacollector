var mongoose = require('mongoose');
var ExchangeDetails = require('./ExchangeDetails');

var ExchangeInfoSchema = mongoose.Schema({

    exchange_name: {
        type: String
    },
    exchange_volume: {type: Number},
    exchange_details: [{
        exchange_details_id: {
            type: mongoose.Schema.ObjectId, ref: 'exchange_details'
        },
        created_at: {type: Number, default: getUnixTimeStamp}
    }],

});

var ExchangeData = module.exports = mongoose.model('exchange_info', ExchangeInfoSchema);






module.exports.findExchangeWithName = function (exchangename, callback) {

    var query = {exchange_name: exchangename};
    ExchangeData.findOne(query, function (error, exchange_data) {

        if (error) {
            //console.log("Error while searching exchange name " + error);
        } else {

            if (exchange_data) {
                ///   console.log("Exchange does exist");
                return callback(null, exchange_data);
            } else {
                return callback(null, null);
            }
        }

    });
};

module.exports.insertInAlreadyExistingDB = function (insert_new_data, ex_name, callback) {
    var query = {
        exchange_name: ex_name,
    }
    var inc_value_by1 = {$inc: {exchange_minute_data: 1}};
    var update_data = {
        $push: {exchange_details: insert_new_data}
    }
    ExchangeData.findOneAndUpdate(query, update_data, function (err, new_data_saved) {
        if (!err) {

            console.log("Inserted document successfully and the data is " + new_data_saved);
            return callback(err, new_data_saved);
        } else {
            console.log("Error while inserting :: " + err);
            return callback(err, new_data_saved);
        }
    });
};

module.exports.createDataExchange = function (data_details_entry, callback) {

    var ex_name = data_details_entry.exchange_name;


    ExchangeData.findExchangeWithName(ex_name, function (error, exchange_data) {
        if (error === null) {

            console.log("exchange data " + exchange_data)

            var exchange_details_save = {
                exchagne_symbol: data_details_entry.exchange_symbol,
                exchange_low_price: data_details_entry.exchange_low_price,
                exchange_high_price: data_details_entry.exchange_high_price,
                exchange_trade_volume: data_details_entry.exchange_trade_volume,
                exchange_btc_price: data_details_entry.exchange_btc_price,
                exchange_hit_timestamp: data_details_entry.exchange_hit_timestamp
            };


            if (!exchange_data) {
                // create a new db in the database

                //console.log("Exchange Don't exist in the db" + exchange_details_save);


                ExchangeDetails.InsertExchangeDetails(exchange_details_save, function (err, saved_details) {

                    //   console.log("Saving data to the db :: " + saved_details)

                    if (!err) {


                        var exchange_name_data = {
                            exchange_name: data_details_entry.exchange_name,
                            exchange_details: saved_details,
                        };

                        var ex_save_data = new ExchangeData(exchange_name_data);
                        ex_save_data.save(function (err, saved_data_indb) {
                            if (!err) {
                                console.log("Saved data :: " + saved_data_indb);
                                ExchangeData.CalculateVolumeAndUpdate(saved_data_indb.exchange_name, function (is_saved) {
                                    if (is_saved) {
                                        console.log("Save data volume");
                                        return callback(saved_data_indb);
                                    } else {
                                        console.log("Not able to save data volume");
                                        return callback(saved_data_indb);
                                    }
                                });
                            } else {
                                console.log("Error :: " + err);
                            }
                        });
                    } else {
                        console.log("Error :: " + err);
                    }
                });
            } else {
                ExchangeDetails.InsertExchangeDetails(exchange_details_save, function (err, saved_details) {
                    if (!err) {
                        ExchangeData.insertInAlreadyExistingDB(saved_details, ex_name, function (err, saved_data) {
                            if (!err) {
                                console.log("I am peacock :: " + saved_data + " saved details" + saved_details)
                                ExchangeData.CalculateVolumeAndUpdate(saved_data.exchange_name, function (isSaved) {
                                    if (isSaved) {
                                        console.log("Save data volume");
                                        return callback(saved_data);
                                    } else {
                                        console.log("Not able to save data volume");
                                        return callback(saved_data);
                                    }
                                })

                            } else {
                                console.log("Error " + err);
                            }

                        });

                    } else {
                        console.log("Error :: " + err);
                    }
                });
            }
        }
    });
};


module.exports.GettingAllData = (callback) => {
    ExchangeData.find({}, function (err, response_data) {
        if (!err)
            return callback(response_data);
    })
}


module.exports.GettingLatestExchangeDetails = function (callback) {
    const promise_arr = [];
    var query = {};
    ExchangeData.find(query, function (err, response_Data) {
        if (!err) {
            var jsonArray = response_Data;
            if (jsonArray.length > 0) {
                for (var i = 0; i < jsonArray.length; i++) {
                    var singleJsonObject = jsonArray[i];
                    var exchange_name = singleJsonObject.exchange_name;
                    var ex_details_jsonarray = singleJsonObject.exchange_details;

                    var ex_id = ex_details_jsonarray[ex_details_jsonarray.length - 1]._id;

                    var single_promise = getExchangeData(exchange_name, ex_id);
                    console.log("single promise " + single_promise);
                    if (single_promise !== undefined)
                        promise_arr.push(single_promise);
                }
                console.log("Promise array " + promise_arr);
                Promise.all(promise_arr)
                    .catch(err => {
                        console.log("Error :: " + err.message);
                    })
                    .then(callback);
            } else {
                console.log("Nothing in DB to work on");
            }

        } else {
            console.log("Error :: " + err.message);
        }
    });
};


function getExchangeData(exchange_name, ex_id) {

    return new Promise(resolve => {
        ExchangeDetails.GiveLatestData(ex_id, function (exchange_details) {

            var exchange_symbol = exchange_name + "_" + exchange_details.exchagne_symbol;

            var exchange_ask_price = exchange_details.all_ask;

            // console.log("details " + JSON.stringify(exchange_ask_price));

            var quantity_sum = 0;
            var return_value = {};

            if (exchange_ask_price.length !== 0) {
                for (var i = 0; i < exchange_ask_price.length; i++) {
                    quantity_sum += exchange_ask_price[i].market_quantity;

                    if (quantity_sum >= 2) {

                        return_value = {
                            exchange_market: exchange_symbol,
                            btc_price: exchange_ask_price[i].market_price
                        }

                    }

                }

                console.log(" ::: " + JSON.stringify(return_value));
                return resolve(return_value);
            } else {
                return resolve({});
            }


        });
    });
}

function getUnixTimeStamp() {
    return Math.floor(new Date().getTime() / 1000);
}


module.exports.CalculateVolumeAndUpdate = function (exchange_name, callback) {
    var query = {exchange_name: exchange_name.toLowerCase()};
    ExchangeData.findOne(query, function (err, exchange_data) {
        if (!err) {
            const promise_arr = [];
            console.log("Data to be saved " + exchange_data);

            for (var i = 0; i < exchange_data.exchange_details.length; i++) {
                const p = new Promise(resolve => {
                    ExchangeDetails.FindVolumeOfExchange(exchange_data.exchange_details[i]._id, function (volume_received) {
                        console.log("Received volume " + volume_received);
                        resolve(volume_received);
                    });
                });

                promise_arr.push(p);
            }
            return Promise.all([Promise.resolve(exchange_name), Promise.all(promise_arr)])
                .catch(err => {
                    console.error("Error " + err.message);
                })
                .then(resolved_data => {
                    console.log("Resolved data :: " + JSON.stringify(resolved_data));
                    let cal = 0;
                    var array_data = resolved_data[1];
                    for (var i = 0; i < array_data.length; i++) {
                        cal = cal + parseFloat(array_data[i]);
                    }
                    console.log(" ---> ", array_data + "---- >> " + cal / array_data.length);
                    var average_volume = cal / array_data.length;

                    var query = {
                        exchange_name: resolved_data[0],
                    };
                    var update_data = {
                        $set: {exchange_volume: average_volume}
                    };

                    ExchangeData.findOneAndUpdate(query, update_data, function (err, new_data_saved) {
                        if (!err) {
                            console.log("Changed Volume successfully");
                            return callback(true);
                        } else {
                            console.log("Error while updating volume" + err);
                            return callback(false);
                        }
                    });
                });
        } else {
            console.log("Error :: " + err.message);
        }
    });

};

module.exports.findexchange_saveBidAsk = function (exchange_name, ask_price, bid_price, callback) {
    var query = {exchange_name: exchange_name.toLowerCase()};
    ExchangeData.findOne(query, function (error, exchange_data) {
        if (error) {
            console.log("Error while searching exchange name " + error.message);
        } else {
            if (exchange_data) {
                var exchange_id = exchange_data.exchange_details[exchange_data.exchange_details.length - 1]._id;
                ExchangeDetails.UpdatingAskCurrentAvailableAsk(exchange_id, ask_price, function (error, all_done) {
                    if (!error) {
                        ExchangeDetails.UpdatingBidsCurrentAvailableBids(exchange_id, bid_price, function () {
                            if (!error) {
                                callback(true);
                            } else {
                                callback(false);
                            }
                        });

                    } else {
                        console.log("Error :: " + error.message);
                        if (bid_price.length > 0) {
                            ExchangeDetails.UpdatingBidsCurrentAvailableBids(exchange_id, bid_price, function () {
                                if (!error) {
                                    callback(true);
                                } else {
                                    callback(false);
                                }
                            });
                        }
                    }
                });
            } else {
                return callback(false);
            }
        }

    });

};
