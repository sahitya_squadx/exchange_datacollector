var mongoose = require('mongoose');

var exchange_data_info = mongoose.Schema({

    exchange_minute_data: {
        type: Number, default: 0
    },
    exchagne_symbol: {
        type: String
    },
    exchange_low_price: {
        type: String
    },
    exchange_high_price: {
        type: String
    },
    exchange_trade_volume: {
        type: String
    },
    exchange_btc_price: {
        type: String
    },
    exchange_hit_timestamp: {
        type: String
    },
    average_volume_till_now: {
        type: Number
    },
    all_ask: [{
        market_price: Number,
        market_quantity: Number
    }],
    all_bids: [{
        market_price: Number,
        market_quantity: Number
    }]
});

var ExchangeDetails = module.exports = mongoose.model('exchange_details', exchange_data_info);

module.exports.InsertExchangeDetails = function (exchange_details, callback) {
    var exchange_data_save = new ExchangeDetails(exchange_details);
    exchange_data_save.save(callback);
};


module.exports.GiveBidAsk = (entry_id, callback) => {
    ExchangeDetails.findOne({_id: entry_id}, function (error, data) {
        if (!error) {
            //console.log("Data here " + JSON.stringify(data));
            return callback({
                asks: data.all_ask,
                bids: data.all_bids
            });
        } else {
            console.log("Error :: " + error.message);
        }
    })
};


module.exports.FindVolumeOfExchange = function (entry_id, callback) {
    var query = {_id: entry_id};
    ExchangeDetails.findOne(query, function (error, received_data) {
        if (error)
            console.log("Error " + error.method);
        return callback(received_data.exchange_trade_volume);
    })
};


module.exports.GiveLatestData = function (entry_id, callback) {
    var query = {_id: entry_id};
    ExchangeDetails.findOne(query, function (error, data) {
        if (!error) {
            return callback(data);
        } else {
            console.log("Error :: " + error.message);
            return null;
        }
    })
};


module.exports.GiveLatestDataWithExchangeName = function (entry_id, exchange_name, callback) {
    //console.log("Exchange Name :: " + exchange_name + " Entry id :: " + entry_id);
    var query = {_id: entry_id};
    ExchangeDetails.findOne(query, function (error, data) {
        if (!error) {
        //    console.log("Data geetting" + data);
            return callback(data, exchange_name);
        } else {
            console.log("Error :: " + error.message);
            return null;
        }
    })
};

module.exports.UpdatingBidCurrentAvailable = function (document_id, bid_prices, callback) {

    for (var i = 0; i < bid_prices.length; i++) {
        var query = {_id: document_id};

        var single_price = bid_prices[i];

        var new_data = {
            market_price: single_price[0],
            market_quantity: single_price[1]
        };


        var update_query = {$push: {all_bids: new_data}};

        var options = {
            upsert: true,
            new: true
        };


        ExchangeDetails.findOneAndUpdate(query, update_query, options, function (error, result) {
            if (!error) {
                if (result) {
                    if (i == bid_prices.length) {
                        callback(error, true);
                    }

                } else {
                    if (i == bid_prices.length) {
                        callback(error, false);
                    }

                }

            } else {
                console.error("Error message :: " + error.message);
                if (i == bid_prices.length)
                    callback(error, false);
            }
        });

    }
};

module.exports.UpdatingAskCurrentAvailableAsk = function (document_id, ask_prices, callback) {


    for (var i = 0; i < ask_prices.length; i++) {

        var query = {_id: document_id};

        var single_price = ask_prices[i];

        var new_data = {
            market_price: single_price[0],
            market_quantity: single_price[1]
        };


        var update_query = {$push: {all_ask: new_data}};

        var options = {
            upsert: true,
            new: true
        };

        ExchangeDetails.findOneAndUpdate(query, update_query, options, function (error, result) {
            if (!error) {
                if (result) {
                    //console.log("Result after updating " + JSON.stringify(result));
                    if (i == ask_prices.length) {
                        //console.log("Successfully inserted data into doc :: " + result)
                        callback(error, true);
                    }

                } else {
                    if (i == ask_prices.length) {
                        callback(error, false);
                    }

                }

            } else {
                console.error("Error message :: " + error.message);
                if (i == ask_prices.length)
                    callback(error, false);
            }
        });

    }


};


module.exports.UpdatingBidsCurrentAvailableBids = function (document_id, bids_prices, callback) {


    for (var i = 0; i < bids_prices.length; i++) {

        var query = {_id: document_id};

        var single_price = bids_prices[i];

        var new_data = {
            market_price: single_price[0],
            market_quantity: single_price[1]
        };


        var update_query = {$push: {all_bids: new_data}};

        var options = {
            upsert: true,
            new: true
        };

        ExchangeDetails.findOneAndUpdate(query, update_query, options, function (error, result) {
            if (!error) {
                if (result) {
                    //console.log("Result after updating " + JSON.stringify(result));
                    if (i == bids_prices.length) {
                        //console.log("Successfully inserted data into doc :: " + result)
                        callback(error, true);
                    }

                } else {
                    if (i == bids_prices.length) {
                        callback(error, false);
                    }

                }

            } else {
                console.error("Error message :: " + error.message);
                if (i == bids_prices.length)
                    callback(error, false);
            }
        });

    }


};

