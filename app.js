var createError = require('http-errors');
var express = require('express');
var http = require('http');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var asTable = require('as-table');
var httpsProxyAgent = require('https-proxy-agent');

const proxy = 'http://13.229.243.235:8001';
const agent = new httpsProxyAgent(proxy);
var orderbook = require('./orderbook');

var Constants = require('./utils/Constants');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var ccxt = require('ccxt');


var graphlib = require("graphlib");
var Graph = graphlib.Graph;

var DataSaving = require('./models/DataSaving');

var mongo = require('mongodb');
var mongoose = require('mongoose');
var db = mongoose.connection;

var app = express();
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/exchange_data");


var exchange_graph = new Graph({directed: true});


function plot_graph_create_path(market_list) {
    console.log("Market list to upload :: " + JSON.stringify(market_list));
    for (var r = 0; r < market_list.length; r++) {
        exchange_graph.setNode(market_list[r].exchange_market);
    }


    for (var i = 0; i < market_list.length; i++) {
        for (var j = 0; j < market_list.length; j++) {
            if (i !== j) {
                var first_market = market_list[i].exchange_market;
                var second_market = market_list[j].exchange_market;
                if (market_list[i].btc_price > market_list[j].btc_price)
                    exchange_graph.setEdge(first_market, second_market, market_list[i].btc_price - market_list[j].btc_price);
                else
                    exchange_graph.setEdge(first_market, second_market, market_list[j].btc_price - market_list[i].btc_price);
            }
        }
    }

    var graph_object = graphlib.alg.dijkstra(exchange_graph, market_list[0].exchange_market, function (e) {
        return exchange_graph.edge(e);
    });

    console.log("--- Graph Drawn " + JSON.stringify(graph_object));

}


const log = require('ololog');
require('ansicolor').nice;

let symbols = ["BTC/USD", "BTC/USDT", "USDT/BTC", "USD/BTC",
    "XBT/USD", "XBT/USDT", "USD/XBT", "USDT/XBT",
    "BTCUSD", "BTCUSDT", "USDBTC", "USDTBTC",
    "XBTUSD", "XBTUSDT", "USDXBT", "USDTXBT"];

let exchanges = ccxt.exchanges;


let promiseArr = [];


//indexRouter.creatingOrderFromExchange();


async function creteOrderFromExchange() {

    console.log("Here it is");

    //const {symbol,// exchangeId, amount, price} = request_params;
    const symbol = 'ETH/BTC';
    const orderType = 'limit';
    const side = 'buy';
    const amount = "0.00001";
    const price = 2;


    const bittrex_exchange = new ccxt.bittrex({
        'apiKey': Constants.BITTREX_API_KEY,
        'secret': Constants.BITTREX_SECRET_KEY,
        'verbose': false,
        'timeout': 60000,
        'enableRateLimit': true,
    });

    // Promise.resolve(bittrex_exchange.cancelOrder(id, symbol))
    //     .catch(err => {
    //         console.log("error " + err.message);
    //     })
    //     .then(order_cancel_data => {
    //         console.log("Cancel order data " + order_cancel_data);
    //     });


    try {

        console.log("Loading market");
        await bittrex_exchange.loadMarkets();
    } catch (e) {
        if (e instanceof ccxt.RequestTimeout)
            console.log(bittrex_exchange.iso8601(Date.now()), e.constructor.name, e.message)
    }


    try {

        var exchange_market_arry = bittrex_exchange.fetchMarkets();

        Promise.resolve(exchange_market_arry)
            .catch(console.log)
            .then(data => {
                var marketarray = [];
                for (var i = 0; i < data.length; i++) {
                    marketarray.push(data[i].symbol);

                    if (data[i].symbol === symbol) {
                        console.log("Market symbols ::" + data[i].symbol + " Length" + data.length);
                        return {market: symbol, position: i, allmarket: marketarray};
                        break;
                    }

                }


            })
            .then(async (data_received) => {

                const response = await bittrex_exchange.createOrder(symbol, orderType, side, amount, price);
                console.log(response);
                console.log('Succeeded');
                console.log("data received  " + JSON.stringify(data_received));
            })

        // Promise.all(exchange_market_arry).catch(err => {
        //     console.log("Error " + err);
        // }).then(data => {
        //     console.log("Data received " + data);
        // })

        // console.log("fetched market", exchange_market_arry);
        //
        // if (exchange_market_arry.length > 0) {
        //     console.log("Bittrex exchange markets ", exchange_market_arry);
        //
        // } else {
        //
        // }


    } catch (e) {
        console.log(bittrex_exchange.iso8601(Date.now()), e.constructor.name, e.message)
        console.log('Failed');
    }
}

creteOrderFromExchange();


var minutes = 1, the_interval = minutes * 60 * 1000;


(function () {
    console.log("Started bro");

    DataSaving.CalculateVolumeAndUpdate("kraken", function (volume_work) {
        console.log("Data received " + volume_work);
    });


    for (let i = 0; i < exchanges.length; i++) {
        let id = exchanges[i];
        const exchange = new ccxt[id]({agent});
        if (exchange.has.publicAPI && exchange.has.fetchOHLCV) {
            const p = get_value_data(exchange, symbols, id);
            p.catch(e => {
                console.trace(e);
                Promise.resolve([]).catch(err => {
                    log("Error while resolving promise :: ".red + err.message);
                })
            });
            promiseArr.push(p);
        } else {
            // console.log("Not following trading view")
        }
    }
    if (promiseArr !== undefined && promiseArr.length > 0)
        resolve_all_promises_atonce(promiseArr);
})();

//}, the_interval);


function get_market_data(exchange, _symbol, _time, id) {
    return Promise.all([
        Promise.resolve(id),
        Promise.resolve(_symbol),
        exchange.fetchOHLCV(_symbol, _time).catch(err => {
            //log("Error while fetching OHLCV ".red + err);
        })
    ]).catch(err => {
        console.log("Error while getting market data " + err.message);
    });
}


async function load_market_data(exchange) {
    await exchange.loadMarkets().catch(err => {
        console.log("Error while loading class " + err.message);
    });
    return exchange;
}

async function load_orderbook_data(exchange, symbol) {

    var orderbook = await exchange.fetchOrderBook(symbol).catch(err => {
        console.log("Error while fetching orderbook" + err.message);
    });

    return orderbook;
}

function save_data_toDB(data) {

    return new Promise((resolve, reject) => {

        if (data !== undefined && data.length > 0) {

            const exchange_symbol = data[1] !== undefined ? data[1] : "not found";
            const exchange_name = data[0] !== undefined ? data[0] : "not found";
            const ohlcv = data.length > 2 && data[2] !== undefined ? data[2] : "not found";

            if (ohlcv && ohlcv !== "not found" && Array.isArray(ohlcv)) {
                const btc_price = ohlcv[ohlcv.length - 1] && ohlcv[ohlcv.length - 1].length > 4 && ohlcv[ohlcv.length - 1][4] !== undefined ? ohlcv[ohlcv.length - 1][4] : "not found"; // closing price
                const volume = ohlcv[ohlcv.length - 1] && ohlcv[ohlcv.length - 1].length > 5 && ohlcv[ohlcv.length - 1][5] !== undefined ? ohlcv[ohlcv.length - 1][5] : "not found";
                const timestamp = ohlcv[ohlcv.length - 1] && ohlcv[ohlcv.length - 1].length > 0 && ohlcv[ohlcv.length - 1][0] !== undefined ? ohlcv[ohlcv.length - 1][0] : "not found";
                const high_price = ohlcv[ohlcv.length - 1] && ohlcv[ohlcv.length - 1].length > 2 && ohlcv[ohlcv.length - 1][2] !== undefined ? ohlcv[ohlcv.length - 1][2] : "not found";
                const low_price = ohlcv[ohlcv.length - 1] && ohlcv[ohlcv.length - 1].length > 3 && ohlcv[ohlcv.length - 1][3] !== undefined ? ohlcv[ohlcv.length - 1][3] : "not found";


                var saving_data_to_db = {
                    exchange_name: exchange_name,
                    exchange_symbol: exchange_symbol,
                    exchange_low_price: low_price,
                    exchange_high_price: high_price,
                    exchange_trade_volume: volume,
                    exchange_btc_price: "$" + btc_price,
                    exchange_hit_timestamp: timestamp
                };

                DataSaving.createDataExchange(saving_data_to_db, function (_saved_data) {
                    if (!_saved_data) {
                        reject(_saved_data);
                    }
                    return resolve(_saved_data);
                });
            } else {
                return resolve({});
            }
        }
    }).catch(err => {
        log('Error while saving data in DB '.red, err.message);
    });
}

function resolve_all_promises_atonce(promiseArr) {

    Promise.all(promiseArr)
        .catch(er => console.log('Error :: ', er))
        .then(data => {
            var saved_exchange_promises = [];
            for (let i = 0; i < data.length; i++) {
                const d = data[i];
                console.log("D length " + d.length);
                if (d.length > 1) {
                    const p = save_data_toDB(d);
                    saved_exchange_promises.push(p);
                }
            }
            return Promise.all(saved_exchange_promises)
                .catch(console.log);
        })
        .then(async function (saved_exchanges) {

            log("Ask portion started".blue + " Exchanges ".red + saved_exchanges.length);
            const promiseArr = [];

            for (var i = 0; i < saved_exchanges.length; i++) {


                console.log("-- " + i + "Value of i :: ", saved_exchanges[i].exchange_name);

                if (saved_exchanges[i].exchange_name !== undefined) {


                    let id = saved_exchanges[i].exchange_name;

                    const exchange = new ccxt[id]({agent});


                    let exchangeFound = ccxt.exchanges.indexOf(id) > -1;

                    if (exchangeFound && exchange.has.publicAPI) {
                        var loaded_exchange_market = await load_market_data(exchange);

                        let foundSymbol = false;

                        for (let index in symbols) {
                            if (loaded_exchange_market.symbols !== undefined && loaded_exchange_market.symbols.includes(symbols[index])) {
                                foundSymbol = symbols[index];
                                break;
                            }
                        }

                        console.log("Found Symbol ::" + foundSymbol);

                        try {


                            if (foundSymbol in exchange.markets) {

                                var p = new Promise(async (reject, resolve) => {

                                    var order_book = await load_orderbook_data(exchange, foundSymbol).catch(err => {
                                        console.log("Error while loading order book" + err.message);

                                    });

                                    let ask_quantity = 0, bid_quantity = 0;
                                    let ask_slice_no = 0, bid_slice_no = 0;
                                    for (var i = 0; i < order_book.asks.length; i++) {
                                        ask_quantity += order_book.asks[i][1];
                                        if (ask_quantity >= 5) {
                                            ask_slice_no = i;
                                            break;
                                        }
                                    }


                                    for (var j = 0; j < order_book.bids.length; j++) {
                                        bid_quantity += order_book.bids[i][1];
                                        if (bid_quantity >= 5) {
                                            bid_slice_no = i;
                                            break;
                                        }
                                    }

                                    var ask_orders = order_book.asks.slice(0, ask_slice_no + 1);
                                    var bid_orders = order_book.bids.slice(0, bid_slice_no + 1);

                                    DataSaving.findexchange_saveBidAsk(exchange.name, ask_orders, bid_orders, function (isDataSaved) {
                                        if (isDataSaved) {
                                            return;
                                        } else {
                                            console.log("Data is not saved ");
                                            return;
                                        }
                                    });


                                });

                                promiseArr.push(p);


                            } else {
                                log('Exchange symbol doesnt not exist in market');
                            }

                        } catch (e) {
                            console.log("Error : " + e.message);
                        }

                    } else {
                        log('Out of League');
                    }
                }

            }
            Promise.all(promiseArr).catch(err => {
                if (err) {
                    log('Error ' + err.message);
                }
            });


        }).then(() => {

        DataSaving.GettingLatestExchangeDetails(function (return_details) {
            var market_list = [];
            console.log("Returned value ", return_details);
            if (return_details !== undefined && return_details.length > 0) {
                for (var i = 0; i < return_details.length; i++) {
                    if (return_details[i].exchange_market && return_details[i].exchange_market !== undefined)
                        market_list.push(return_details[i]);
                }
                plot_graph_create_path(market_list);
            }
        });

    });
}

async function get_value_data(exchange, symbols, id) {

    if (exchange.has.publicAPI) {
        try {
            await exchange.loadMarkets().catch(err => {
                //console.log("Error loading market " + err.message);
            });
        } catch (e) {
            return Promise.resolve([]).catch(error => {
                // log('Error while getting error ', error.message);
            });
        }


        let foundSymbol = false;
        //
        for (let index in symbols) {
            if (exchange.symbols !== undefined && exchange.symbols.includes(symbols[index])) {
                foundSymbol = symbols[index];
                break;
            }
        }

        if (foundSymbol && exchange.has.fetchOHLCV) {
            return get_market_data(exchange, foundSymbol, "1m", id)
                .catch(er => {
                    console.log("Error :: " + er.message);
                    return Promise.resolve([]).catch(error => {
                        console.log("Error " + error.message);
                    });
                });
        }
        else {
            return Promise.resolve([]).catch(error => {
                console.log("Error " + error.message);
            });
        }

    } else return Promise.resolve([]).catch(error => {
        console.log("Error " + error.message);
    });

}


app.use('/', indexRouter);
app.use('/users', usersRouter);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(function (req, res, next) {
    next(createError(404));
});

app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


app.listen(Constants.ServerPortNot, Constants.ServerDevelopmentIP, function (err) {
    if (!err) {
        console.log('Server is running at ' + Constants.ServerDevelopmentIP + ":" + Constants.ServerPortNot);
    } else {
        console.log("Err " + err.message);
    }

});

module.exports = app;